package controller;

import view.MainView;
import view.ReadView;
import view.UpdateView;
import view.InsertView;
import model.dto.Cliente;
import model.service.MainService;
import view.DeleteView;

public class MainController {
	static MainView mainView;
	private static InsertView insertView;
	static ReadView readView;
	static UpdateView updateView;
	static DeleteView deleteView;
	
	public static void setMainView(MainView mainView) {
		MainController.mainView = mainView;
	}
	
	public static void setInsertView(InsertView insertView) {
		MainController.insertView = insertView;
	}
	
	public static void setReadView(ReadView readView) {
		MainController.readView = readView;
	}
	
	public static void setUpdateView(UpdateView updateView) {
		MainController.updateView = updateView;
	}
	
	public static void setDeleteView(DeleteView deleteView) {
		MainController.deleteView = deleteView;
	}
	
	public static void showInsertView() {
		insertView.setVisible(true);
	}
	
	public static void showReadView() {
		readView.setVisible(true);
	}
	
	public static void showUpdateView() {
		updateView.setVisible(true);
	}
	
	public static void showDeleteView() {
		deleteView.setVisible(true);
	}
	
	
	public static void insertCliente(Cliente newCliente) {
		MainService.validateInsert(newCliente);
	}

	public static void updateCliente(Cliente newCliente) {
		MainService.validateUpdate(newCliente);
	}
	
	public static void readCliente(Cliente newCliente) {
		MainService.validateRead(newCliente);
	}
	
	public static void deleteCliente(Cliente newCliente) {
		MainService.validateDelete(newCliente);
	}
}
