import view.MainView;
import view.ReadView;
import view.UpdateView;
import view.InsertView;
import controller.MainController;
import view.DeleteView;

public class mainApp {
	
	MainView mainView;
	InsertView insertView;
	ReadView readView;
	UpdateView updateView;
	DeleteView deleteView;

	public static void main(String[] args) {
		mainApp mainApp=new mainApp();
		mainApp.iniciar();
	}

	private void iniciar() {
		mainView=new MainView();
		insertView=new InsertView();
		readView= new ReadView();
		updateView=new UpdateView();
		deleteView= new DeleteView();
		
		MainController.setMainView(mainView);
		MainController.setInsertView(insertView);
		MainController.setReadView(readView);
		MainController.setUpdateView(updateView);
		MainController.setDeleteView(deleteView);
		
		mainView.setVisible(true);
	}
}
